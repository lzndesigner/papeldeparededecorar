<?php

/* base/template/common/footer.twig */
class __TwigTemplate_890d27c3c59ff7c753029458b9614ff01320f68f3249344fcb9a864cc5f84d6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "</main>
<div class=\"footer-desktop\">
  <footer>
    <div class=\"container\">
      <div class=\"row\">
      <div class=\"col-sm-6\">
        <h5>Sobre</h5>
        <p>
          A Decorar Papel de Parede iniciou seus trabalhos em 2014 e hoje se consolida como uma das melhores empresas de papel de parede da grande São Paulo.
        </p>
        <p>
          Visando sempre o melhor para nossos clientes, trabalhamos constantemente no nosso desenvolvimento, seja em busca de opções mais modernas, mais resistentes e duráveis, até no aprimoramento da aplicação do papel.
        </p>
      </div>
        ";
        // line 15
        if ((isset($context["informations"]) ? $context["informations"] : null)) {
            // line 16
            echo "        <div class=\"col-sm-3\">
          <h5>";
            // line 17
            echo (isset($context["text_information"]) ? $context["text_information"] : null);
            echo "</h5>
          <ul class=\"list-unstyled links-footer\">
           ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 20
                echo "           <li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "         </ul>
       </div>
       ";
        }
        // line 25
        echo "       <div class=\"col-sm-3\">
        <h5>Atendimento ao Cliente</h5>
        <ul class=\"list-unstyled links-footer\">
          <li><a href=\"";
        // line 28
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>
          <li><a href=\"";
        // line 29
        echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
        echo "\">";
        echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
        echo "</a></li>
        </ul>
      </div>
    </div>
  </div><!-- container -->
  ";
        // line 34
        if (((((isset($context["facebook_status"]) ? $context["facebook_status"] : null) && twig_length_filter($this->env, (isset($context["facebook_url"]) ? $context["facebook_url"] : null))) && (isset($context["instagram_status"]) ? $context["instagram_status"] : null)) && twig_length_filter($this->env, (isset($context["instagram_url"]) ? $context["instagram_url"] : null)))) {
            // line 35
            echo "  <div class=\"bgsocial-midia\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"col-md-3\"><h5>Encontre-nos aqui</h5></div>
          <div class=\"col-md-9\">
            <ul class=\"social-icons text-left\">
              ";
            // line 42
            if (((isset($context["facebook_status"]) ? $context["facebook_status"] : null) && twig_length_filter($this->env, (isset($context["facebook_url"]) ? $context["facebook_url"] : null)))) {
                // line 43
                echo "              <li class=\"social-icons-facebook\" data-toggle=\"tooltip\" title=\"Facebook\"><a href=\"";
                echo (isset($context["facebook_url"]) ? $context["facebook_url"] : null);
                echo "\" target=\"_Blank\"><i class=\"fa fa-facebook\"></i></a></li>
              ";
            }
            // line 45
            echo "              ";
            if (((isset($context["instagram_status"]) ? $context["instagram_status"] : null) && twig_length_filter($this->env, (isset($context["instagram_url"]) ? $context["instagram_url"] : null)))) {
                // line 46
                echo "              <li class=\"social-icons-instagram\" data-toggle=\"tooltip\" title=\"Instagram\"><a href=\"https://instagram.com/";
                echo (isset($context["instagram_url"]) ? $context["instagram_url"] : null);
                echo "\" target=\"_Blank\"><i class=\"fa fa-instagram\"></i></a></li>
              ";
            }
            // line 48
            echo "            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- bgsocial-midia -->
  ";
        }
        // line 55
        echo "  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-sm-12 pull-left text-center\">
        <hr>
        <p>";
        // line 59
        echo (isset($context["powered"]) ? $context["powered"] : null);
        echo " By <a href=\"#\" target=\"_Blank\">Kiwimidia</a></p>
      </div>
    </div>
  </div><!-- container -->
</footer>
</div>
<div class=\"footer-mobile\">
  <footer>
    <div class=\"d-flex\">
      <div class=\"flex-fill\">
        <a href=\"index.php?route=common/home\">
          <i class=\"fa fa-home\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a  href=\"javascript:;\" class=\"btnToggleMenu\">
          <i class=\"fa fa-bars\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a href=\"index.php?route=account/account\">
          <i class=\"fa fa-user\"></i>
        </a>
      </div><!-- flex-fill -->
      <div class=\"flex-fill\">
        <a href=\"index.php?route=checkout/cart\" id=\"cart-footer\">
          <i class=\"fa fa-shopping-cart\"></i>
          <span id=\"cart-total\">";
        // line 86
        echo (isset($context["text_items"]) ? $context["text_items"] : null);
        echo "</span>
        </a>
      </div><!-- flex-fill -->
    </div>
  </footer>
</div>
";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 93
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "</body></html>";
    }

    public function getTemplateName()
    {
        return "base/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 95,  172 => 93,  168 => 92,  159 => 86,  129 => 59,  123 => 55,  114 => 48,  108 => 46,  105 => 45,  99 => 43,  97 => 42,  88 => 35,  86 => 34,  76 => 29,  70 => 28,  65 => 25,  60 => 22,  49 => 20,  45 => 19,  40 => 17,  37 => 16,  35 => 15,  19 => 1,);
    }
}
/* </main>*/
/* <div class="footer-desktop">*/
/*   <footer>*/
/*     <div class="container">*/
/*       <div class="row">*/
/*       <div class="col-sm-6">*/
/*         <h5>Sobre</h5>*/
/*         <p>*/
/*           A Decorar Papel de Parede iniciou seus trabalhos em 2014 e hoje se consolida como uma das melhores empresas de papel de parede da grande São Paulo.*/
/*         </p>*/
/*         <p>*/
/*           Visando sempre o melhor para nossos clientes, trabalhamos constantemente no nosso desenvolvimento, seja em busca de opções mais modernas, mais resistentes e duráveis, até no aprimoramento da aplicação do papel.*/
/*         </p>*/
/*       </div>*/
/*         {% if informations %}*/
/*         <div class="col-sm-3">*/
/*           <h5>{{ text_information }}</h5>*/
/*           <ul class="list-unstyled links-footer">*/
/*            {% for information in informations %}*/
/*            <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*            {% endfor %}*/
/*          </ul>*/
/*        </div>*/
/*        {% endif %}*/
/*        <div class="col-sm-3">*/
/*         <h5>Atendimento ao Cliente</h5>*/
/*         <ul class="list-unstyled links-footer">*/
/*           <li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/*           <li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*     </div>*/
/*   </div><!-- container -->*/
/*   {% if facebook_status and facebook_url|length and instagram_status and instagram_url|length %}*/
/*   <div class="bgsocial-midia">*/
/*     <div class="container">*/
/*       <div class="row">*/
/*         <div class="col-md-12">*/
/*           <div class="col-md-3"><h5>Encontre-nos aqui</h5></div>*/
/*           <div class="col-md-9">*/
/*             <ul class="social-icons text-left">*/
/*               {% if facebook_status and facebook_url|length %}*/
/*               <li class="social-icons-facebook" data-toggle="tooltip" title="Facebook"><a href="{{ facebook_url }}" target="_Blank"><i class="fa fa-facebook"></i></a></li>*/
/*               {% endif %}*/
/*               {% if instagram_status and instagram_url|length %}*/
/*               <li class="social-icons-instagram" data-toggle="tooltip" title="Instagram"><a href="https://instagram.com/{{ instagram_url }}" target="_Blank"><i class="fa fa-instagram"></i></a></li>*/
/*               {% endif %}*/
/*             </ul>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div><!-- bgsocial-midia -->*/
/*   {% endif %}*/
/*   <div class="container">*/
/*     <div class="row">*/
/*       <div class="col-sm-12 pull-left text-center">*/
/*         <hr>*/
/*         <p>{{ powered }} By <a href="#" target="_Blank">Kiwimidia</a></p>*/
/*       </div>*/
/*     </div>*/
/*   </div><!-- container -->*/
/* </footer>*/
/* </div>*/
/* <div class="footer-mobile">*/
/*   <footer>*/
/*     <div class="d-flex">*/
/*       <div class="flex-fill">*/
/*         <a href="index.php?route=common/home">*/
/*           <i class="fa fa-home"></i>*/
/*         </a>*/
/*       </div><!-- flex-fill -->*/
/*       <div class="flex-fill">*/
/*         <a  href="javascript:;" class="btnToggleMenu">*/
/*           <i class="fa fa-bars"></i>*/
/*         </a>*/
/*       </div><!-- flex-fill -->*/
/*       <div class="flex-fill">*/
/*         <a href="index.php?route=account/account">*/
/*           <i class="fa fa-user"></i>*/
/*         </a>*/
/*       </div><!-- flex-fill -->*/
/*       <div class="flex-fill">*/
/*         <a href="index.php?route=checkout/cart" id="cart-footer">*/
/*           <i class="fa fa-shopping-cart"></i>*/
/*           <span id="cart-total">{{ text_items }}</span>*/
/*         </a>*/
/*       </div><!-- flex-fill -->*/
/*     </div>*/
/*   </footer>*/
/* </div>*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* </body></html>*/
