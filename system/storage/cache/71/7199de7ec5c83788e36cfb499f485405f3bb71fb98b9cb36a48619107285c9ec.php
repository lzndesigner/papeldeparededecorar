<?php

/* base/template/common/header.twig */
class __TwigTemplate_33dff8f27bf292e4c8aca955a3f41532e83c757a78d83e26fd739b586d9dc25f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
  <meta charset=\"UTF-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
  <base href=\"";
        // line 13
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
  ";
        // line 14
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 15
            echo "  <meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
  ";
        }
        // line 17
        echo "  ";
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 18
            echo "  <meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
  ";
        }
        // line 20
        echo "  <script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
  <link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
  <script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
  <link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
  <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700\" rel=\"stylesheet\" type=\"text/css\" />
  <link href=\"https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;700;900&display=swap\" rel=\"stylesheet\">
  <link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 27
        echo (isset($context["theme"]) ? $context["theme"] : null);
        echo "/stylesheet/stylesheet.css?";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 28
        echo (isset($context["theme"]) ? $context["theme"] : null);
        echo "/stylesheet/custom.css?";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 29
        echo (isset($context["theme"]) ? $context["theme"] : null);
        echo "/stylesheet/responsive.css?";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  <link href=\"catalog/view/theme/";
        // line 30
        echo (isset($context["theme"]) ? $context["theme"] : null);
        echo "/stylesheet/template-custom.css?";
        echo twig_random($this->env);
        echo "\" rel=\"stylesheet\">
  ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 32
            echo "  <link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 35
            echo "  <script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "  <script src=\"catalog/view/javascript/common.js?";
        echo twig_random($this->env);
        echo "\" type=\"text/javascript\"></script>
  ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 39
            echo "  <link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 42
            echo "  ";
            echo $context["analytic"];
            echo "
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "
  <script src=\"catalog/view/javascript/jquery/jquery.mask.js\" type=\"text/javascript\"></script>
  <script type=\"text/javascript\">
  \$(document).ready(function(){    
    \$('input[name=\"postcode\"').mask('00000-000');
    \$('input[name=\"custom_field[account][1]\"').mask('000.000.000-00', {reverse: true});
    \$('input[name=\"telephone\"').mask(\"(99) 99999-9999\");
    \$('input[name=\"telephone\"').on(\"blur\", function() {
      var last = \$(this).val().substr( \$(this).val().indexOf(\"-\") + 1 );

      if( last.length == 3 ) {
        var move = \$(this).val().substr( \$(this).val().indexOf(\"-\") - 1, 1 );
        var lastfour = move + last;
        var first = \$(this).val().substr( 0, 9 );

        \$(this).val( first + '-' + lastfour );
      }
    });
  });
  </script>

</head>
<body class=\"";
        // line 66
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
  <div class=\"header-desktop\">
    <header id=\"headerDesktop\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-3\">
            <div id=\"logo\">
              ";
        // line 73
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 74
            echo "              <a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive logoWhite\" /><img src=\"/catalog_base_oc3/image/catalog/logo_decorar_papel_de_parede.png\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive logoColor\" /></a>
              ";
        } else {
            // line 76
            echo "              <h1><a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "</a></h1>
              ";
        }
        // line 78
        echo "            </div>
          </div>
          <div class=\"col-sm-9\">
            <div class=\"row\">
              <div class=\"col-sm-12\">
                ";
        // line 83
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo " 
              </div>
              <div class=\"col-sm-6 pull-right\">";
        // line 85
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "</div>                
            </div><!-- row -->
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container -->
    </header>
  </div><!-- header-desktop -->

  <div class=\"header-mobile\">
    <nav id=\"top\" class=\"mobile-top\">
      <div class=\"container\">
        <div class=\"d-flex\">
          <div class=\"\">
            <a href=\"javascript:history.go(-1);\" class=\"btn\"><i class=\"fa fa-angle-left\"></i></a>
          </div>            
          <div class=\"\">
            <a href=\"index.php?route=account/account\" class=\"btn-user\"><i class=\"fa fa-user\"></i></a>
          </div>
          <div class=\"\">
            ";
        // line 104
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
          </div>
          <div class=\"ml-auto\">
            <a href=\"javascript:;\" class=\"btn btnToggleMenu\"><i class=\"fa fa-bars\"></i></a>
          </div>
        </div>
      </div>
    </nav>
    <header id=\"headerMobile\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-12\">
            <div id=\"logo\">
              ";
        // line 117
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 118
            echo "              <a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive logoWhite\" /><img src=\"/catalog_base_oc3/image/catalog/logo_decorar_papel_de_parede.png\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive logoColor\" /></a>
              ";
        } else {
            // line 120
            echo "              <h1><a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "</a></h1>
              ";
        }
        // line 122
        echo "            </div>
          </div>
        </div>
      </div>
      <div class=\"container\">
        <div class=\"box-search\">
          ";
        // line 128
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "
        </div>
      </div>
    </header>

    <div class=\"menu-mobile\">
      <a href=\"javascript:;\" class=\"btnCloseToggleMenu\"><i class=\"fa fa-times\"></i></a>
      ";
        // line 135
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "
    </div>
  </div><!-- header-mobile -->

  <main class=\"content_geral\">";
    }

    public function getTemplateName()
    {
        return "base/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 135,  314 => 128,  306 => 122,  298 => 120,  282 => 118,  280 => 117,  264 => 104,  242 => 85,  237 => 83,  230 => 78,  222 => 76,  206 => 74,  204 => 73,  194 => 66,  170 => 44,  161 => 42,  156 => 41,  145 => 39,  141 => 38,  136 => 37,  127 => 35,  122 => 34,  109 => 32,  105 => 31,  99 => 30,  93 => 29,  87 => 28,  81 => 27,  72 => 20,  66 => 18,  63 => 17,  57 => 15,  55 => 14,  51 => 13,  47 => 12,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/*   <meta charset="UTF-8" />*/
/*   <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*   <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*   <title>{{ title }}</title>*/
/*   <base href="{{ base }}" />*/
/*   {% if description %}*/
/*   <meta name="description" content="{{ description }}" />*/
/*   {% endif %}*/
/*   {% if keywords %}*/
/*   <meta name="keywords" content="{{ keywords }}" />*/
/*   {% endif %}*/
/*   <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>*/
/*   <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />*/
/*   <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>*/
/*   <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/*   <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />*/
/*   <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;700;900&display=swap" rel="stylesheet">*/
/*   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">*/
/*   <link href="catalog/view/theme/{{ theme }}/stylesheet/stylesheet.css?{{ random() }}" rel="stylesheet">*/
/*   <link href="catalog/view/theme/{{ theme }}/stylesheet/custom.css?{{ random() }}" rel="stylesheet">*/
/*   <link href="catalog/view/theme/{{ theme }}/stylesheet/responsive.css?{{ random() }}" rel="stylesheet">*/
/*   <link href="catalog/view/theme/{{ theme }}/stylesheet/template-custom.css?{{ random() }}" rel="stylesheet">*/
/*   {% for style in styles %}*/
/*   <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/*   {% endfor %}*/
/*   {% for script in scripts %}*/
/*   <script src="{{ script }}" type="text/javascript"></script>*/
/*   {% endfor %}*/
/*   <script src="catalog/view/javascript/common.js?{{ random() }}" type="text/javascript"></script>*/
/*   {% for link in links %}*/
/*   <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/*   {% endfor %}*/
/*   {% for analytic in analytics %}*/
/*   {{ analytic }}*/
/*   {% endfor %}*/
/* */
/*   <script src="catalog/view/javascript/jquery/jquery.mask.js" type="text/javascript"></script>*/
/*   <script type="text/javascript">*/
/*   $(document).ready(function(){    */
/*     $('input[name="postcode"').mask('00000-000');*/
/*     $('input[name="custom_field[account][1]"').mask('000.000.000-00', {reverse: true});*/
/*     $('input[name="telephone"').mask("(99) 99999-9999");*/
/*     $('input[name="telephone"').on("blur", function() {*/
/*       var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );*/
/* */
/*       if( last.length == 3 ) {*/
/*         var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );*/
/*         var lastfour = move + last;*/
/*         var first = $(this).val().substr( 0, 9 );*/
/* */
/*         $(this).val( first + '-' + lastfour );*/
/*       }*/
/*     });*/
/*   });*/
/*   </script>*/
/* */
/* </head>*/
/* <body class="{{ class }}">*/
/*   <div class="header-desktop">*/
/*     <header id="headerDesktop">*/
/*       <div class="container">*/
/*         <div class="row">*/
/*           <div class="col-sm-3">*/
/*             <div id="logo">*/
/*               {% if logo %}*/
/*               <a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" class="img-responsive logoWhite" /><img src="/catalog_base_oc3/image/catalog/logo_decorar_papel_de_parede.png" title="{{ name }}" alt="{{ name }}" class="img-responsive logoColor" /></a>*/
/*               {% else %}*/
/*               <h1><a href="{{ home }}">{{ name }}</a></h1>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="col-sm-9">*/
/*             <div class="row">*/
/*               <div class="col-sm-12">*/
/*                 {{ menu }} */
/*               </div>*/
/*               <div class="col-sm-6 pull-right">{{ search }}</div>                */
/*             </div><!-- row -->*/
/*           </div><!-- col -->*/
/*         </div><!-- row -->*/
/*       </div><!-- container -->*/
/*     </header>*/
/*   </div><!-- header-desktop -->*/
/* */
/*   <div class="header-mobile">*/
/*     <nav id="top" class="mobile-top">*/
/*       <div class="container">*/
/*         <div class="d-flex">*/
/*           <div class="">*/
/*             <a href="javascript:history.go(-1);" class="btn"><i class="fa fa-angle-left"></i></a>*/
/*           </div>            */
/*           <div class="">*/
/*             <a href="index.php?route=account/account" class="btn-user"><i class="fa fa-user"></i></a>*/
/*           </div>*/
/*           <div class="">*/
/*             {{ cart }}*/
/*           </div>*/
/*           <div class="ml-auto">*/
/*             <a href="javascript:;" class="btn btnToggleMenu"><i class="fa fa-bars"></i></a>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </nav>*/
/*     <header id="headerMobile">*/
/*       <div class="container">*/
/*         <div class="row">*/
/*           <div class="col-sm-12">*/
/*             <div id="logo">*/
/*               {% if logo %}*/
/*               <a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" class="img-responsive logoWhite" /><img src="/catalog_base_oc3/image/catalog/logo_decorar_papel_de_parede.png" title="{{ name }}" alt="{{ name }}" class="img-responsive logoColor" /></a>*/
/*               {% else %}*/
/*               <h1><a href="{{ home }}">{{ name }}</a></h1>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*       <div class="container">*/
/*         <div class="box-search">*/
/*           {{ search }}*/
/*         </div>*/
/*       </div>*/
/*     </header>*/
/* */
/*     <div class="menu-mobile">*/
/*       <a href="javascript:;" class="btnCloseToggleMenu"><i class="fa fa-times"></i></a>*/
/*       {{ menu }}*/
/*     </div>*/
/*   </div><!-- header-mobile -->*/
/* */
/*   <main class="content_geral">*/
