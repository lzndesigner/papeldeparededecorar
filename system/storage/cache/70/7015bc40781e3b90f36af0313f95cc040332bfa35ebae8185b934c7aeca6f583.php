<?php

/* base/template/common/menu.twig */
class __TwigTemplate_ea104e79e678d23d7e50162977d773a6e43003fa0c9419870bdcfe1eb37de6db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav id=\"menu\" class=\"navbar\">
    <!-- 
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
        // line 3
        echo (isset($context["text_category"]) ? $context["text_category"] : null);
        echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
    collapse
  -->
  <div class=\"navbar-collapse navbar-ex1-collapse\">
    <ul class=\"nav navbar-nav\">
      <li><a href=\"";
        // line 10
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\">Início</a></li>
      <li><a href=\"/sobre\">Sobre</a></li>
      ";
        // line 12
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 13
            echo "      <li class=\"dropdown\">
        <a href=\"index.php?route=product/category\">Produtos <span class=\"openMenu d-inline-block d-sm-inline-block d-md-none\"><i class=\"fa fa-angle-down\"></i></span></a>
        <div class=\"dropdown-menu\">
          <div class=\"dropdown-inner\">
            <ul class=\"list-unstyled\">
              ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 19
                echo "              <li><a href=\"";
                echo $this->getAttribute($context["category"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["category"], "name", array());
                echo "</a></li>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "            </ul>
          </div>
        </div>
        <li>
          ";
        }
        // line 25
        echo " 
          <li><a href=\"/contato\">Contato</a></li>
        </ul>
      </div>
    </nav>";
    }

    public function getTemplateName()
    {
        return "base/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 25,  62 => 21,  51 => 19,  47 => 18,  40 => 13,  38 => 12,  33 => 10,  23 => 3,  19 => 1,);
    }
}
/* <nav id="menu" class="navbar">*/
/*     <!-- */
/*     <div class="navbar-header"><span id="category" class="visible-xs">{{ text_category }}</span>*/
/*       <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>*/
/*     </div>*/
/*     collapse*/
/*   -->*/
/*   <div class="navbar-collapse navbar-ex1-collapse">*/
/*     <ul class="nav navbar-nav">*/
/*       <li><a href="{{ home }}">Início</a></li>*/
/*       <li><a href="/sobre">Sobre</a></li>*/
/*       {% if categories %}*/
/*       <li class="dropdown">*/
/*         <a href="index.php?route=product/category">Produtos <span class="openMenu d-inline-block d-sm-inline-block d-md-none"><i class="fa fa-angle-down"></i></span></a>*/
/*         <div class="dropdown-menu">*/
/*           <div class="dropdown-inner">*/
/*             <ul class="list-unstyled">*/
/*               {% for category in categories %}*/
/*               <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*               {% endfor %}*/
/*             </ul>*/
/*           </div>*/
/*         </div>*/
/*         <li>*/
/*           {% endif %} */
/*           <li><a href="/contato">Contato</a></li>*/
/*         </ul>*/
/*       </div>*/
/*     </nav>*/
