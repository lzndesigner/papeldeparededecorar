<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
					);
			}
		}		

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['mail_admin'] = $this->config->get('config_email');

		$data['telephone'] = $this->config->get('config_telephone');
		$data['cellphone'] = $this->config->get('config_fax');
		$retirar = array("(", ")", "-", " ", ".", ",");
		$data['telephoneLink'] = str_replace($retirar, '', $this->config->get('config_telephone'));
		$data['cellphoneLink'] = str_replace($retirar, '', $this->config->get('config_fax'));

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts());

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$data['scripts'] = $this->document->getScripts('footer');

		/* CUSTOM - REDE SOCIAIS */
		$data['facebook_status'] = $this->config->get('config_facebook');
		$data['facebook_url'] = $this->config->get('config_facebook_url');
		$data['instagram_status'] = $this->config->get('config_instagram');
		$data['instagram_url'] = $this->config->get('config_instagram_url');
		$data['instagram_widget'] = $this->config->get('config_instagram_widget');
		
		return $this->load->view('common/footer', $data);
	}

	public function info() {
		$this->response->setOutput($this->index());
	}
}
