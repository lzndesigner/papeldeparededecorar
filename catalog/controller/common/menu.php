<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);


				foreach ($children as $child) {

					if($child['top']){
						$visibleChildCategory = true;
					}else{
						$visibleChildCategory = false;					
					}

					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'visibleChildCategory' => $visibleChildCategory,
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				if($category['top']){
					$visibleCategory = true;
				}else{
					$visibleCategory = false;					
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'image'     => $category['image'],
					'visibleCategory' => $visibleCategory,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			
		}

		return $this->load->view('common/menu', $data);
	}
}
