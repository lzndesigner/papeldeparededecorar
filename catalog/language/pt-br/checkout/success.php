<?php
// Heading
$_['heading_title'] = 'Pedido cadastrado';

// Text
$_['text_basket']   = 'Carrinho de compras';
$_['text_checkout'] = 'Finalizar pedido';
$_['text_success']  = 'Confirmação';
$_['text_customer'] = '<p>O seu pedido foi cadastrado.</p><p>Vamos preparar seu pedido e você será avisado através de sua <a href="%s">conta</a>, acessando o menu <a href="%s">histórico de pedidos</a>.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p>';
$_['text_guest']    = '<p>O seu pedido foi cadastrado.</p><p>Entre em contato com nosso <a href="%s">atendimento</a> caso tenha dúvidas.</p><p>Obrigado por comprar em nossa loja.</p>';